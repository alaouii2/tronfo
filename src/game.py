from enum import Enum, auto
from card import Deck
from player import Player, Team
from globals import PLAYERS_NUMBER, TEAMS_NUMBER, CALLS

class GameStates(Enum):
    INITIALIZE = auto()
    BIDDING = auto()
    CALLING = auto()
    PLAYING = auto()
    GAMEEND = auto()

class Game:
    def __init__(self):
        self.state = GameStates.INITIALIZE
        self.ongoing = True
        self.deck = Deck()
        self.players = [Player() for _ in range(PLAYERS_NUMBER)]
        self.teams = [Team() for _ in range(TEAMS_NUMBER)]
        self.player_calling_index = None
        self.highest_bid = None
        self._tronfo = None
        self.table = [None, None, None, None]
        self.going_first = 0
        self.played_typing = None
        self.highest_typing_played = None
        self.highest_tronfo_played = None

    @property
    def tronfo(self):
        return self._tronfo

    @tronfo.setter
    def tronfo(self, value):
        typings_initials = [t.initial for t in Deck.TYPINGS]
        if value in typings_initials:
            self._tronfo = value
            return
        raise ValueError(f"Invalid typing initial \'{value}\'. Possible values: {typings_initials}")

    def initialize(self):
        self.deck.initialize()
        self.deck.soft_shuffle()
        for i in range(TEAMS_NUMBER):
            self.teams[i].join_players(self.players[i], self.players[i + 2])
        print("Distributing cards ...")
        for i in range(PLAYERS_NUMBER * 2):
            self.players[i % PLAYERS_NUMBER].add_cards_to_hand(self.deck.pick_cards())
        assert not self.deck.cards
        self.state = GameStates.BIDDING

    def get_possible_bids(self):
        if self.highest_bid:
            start = CALLS.index(self.highest_bid) + 1
            return CALLS[start:]
        return CALLS

    def take_bid_from_player(self, player_index: int):
        if self.players[player_index].bid == 'pass':
            return
        
        print(self.players[player_index], ':')
        possible_bids = self.get_possible_bids()
        while True:
            try:
                bid = input("Bid: ")
                if bid not in possible_bids:
                    raise ValueError(f"The bid \'{bid}\' is too low or invalid. Possible bids: {possible_bids}")
                self.players[player_index].bid = bid
                if bid != 'pass':
                    self.highest_bid = bid
                    self.player_calling_index = player_index
                break
            except ValueError as ex:
                print(ex)
        
        if self.players[player_index].bid != 'capo':
            return
        
        for i in range(PLAYERS_NUMBER):
            if i == player_index:
                continue
            self.players[i].bid = 'pass'
    
    def get_all_current_bids(self):
        return [player.bid for player in self.players]

    def bidding(self):
        while True:
            for i in range(len(self.players)):
                self.take_bid_from_player(i)
            bids_list = self.get_all_current_bids()
            if bids_list.count('pass') == 4:
                self.state = GameStates.INITIALIZE
                break
            if bids_list.count('pass') == 3:
                self.state = GameStates.CALLING
                break

    def calling(self):
        print(self.players[self.player_calling_index], ":")
        while True:
            try:
                self.tronfo = input("Call a typing: ")
                break
            except ValueError as ex:
                print(ex)
        self.state = GameStates.PLAYING

    def is_table_empty(self):
        return self.table.count(None) == 4
    
    def clear_table(self):
        self.table = [None, None, None, None]

    def get_valid_plays(self, player):
        if self.is_table_empty():
            return player.hand
        typing_list = player.get_typing_from_hand(self.played_typing)
        print(typing_list)
        tronfo_list = player.get_typing_from_hand(self.tronfo)
        print(tronfo_list)
        if self.highest_tronfo_played:
            print(self.highest_tronfo_played)
            if typing_list:
                return typing_list, 0
            if tronfo_list:
                higher_tronfo_list = [card for card in tronfo_list if card > self.highest_tronfo_played]
                if higher_tronfo_list:
                    return higher_tronfo_list, 1
                return tronfo_list, 0
            return player.hand
        if typing_list:
            higher_typing_list = [card for card in typing_list if card > self.highest_typing_played]
            if higher_typing_list:
                return higher_typing_list, 1
            return typing_list
        if tronfo_list:
            return tronfo_list, 1
        return player.hand, 0
    
    def print_table(self):
        print(f"""
            {self.table[(2 + self.going_first) % 4]}


{self.table[(3 + self.going_first) % 4]}                 Next -> {self.table[(1 + self.going_first) % 4]}



     You -> {self.table[(0 + self.going_first) % 4]}
""")

    def turn(self):
        player_taking = None
        for i in range(PLAYERS_NUMBER):
            self.print_table()
            player = self.players[(self.going_first + i) % 4]
            print(f"{player}:")
            while True:
                print(f"hand: {player.get_hand_repr()}")
                print(f"      {'  '.join([str(i) for i in range(len(player.hand))])}")
                card_played_index_str = input("Choose the number number which corresponds to the card to play:\n> ")
                try:
                    card_played_index = int(card_played_index_str)
                    played_card = player.hand[card_played_index]
                except (ValueError, IndexError):
                    print(f"Invalid choice \'{card_played_index_str}\'.")
                    continue
                valid_plays, taking = self.get_valid_plays(player)
                if played_card not in valid_plays:
                    print(f"The move made is illegal. Legal moves: {self.get_valid_plays(player)}")
                    continue

                
                print(played_card)
                if self.is_table_empty():
                    self.played_typing = played_card.typing.initial
                    print(self.played_typing)
                    player_taking = player
                self.table[i] = played_card
                player.hand.pop(card_played_index)
                if played_card.get_typing() == self.played_typing and taking:
                    self.highest_typing_played = played_card
                    player_taking = player
                if played_card.get_typing() == self.tronfo and taking:
                    self.highest_tronfo_played = played_card
                    player_taking = player
                break
        player_taking.team.add_cards_to_taken(self.table)
        
    def gameplay(self): 
        print(f"{self.players[self.going_first]} is starting.")
        for _ in range(10):
            self.turn()
            self.clear_table()
            self.played_typing = None
            self.highest_typing_played = None
            self.highest_tronfo_played = None
        self.state = GameStates.GAMEEND

    def calculate_total(self):
        self.state = GameStates.INITIALIZE

    def run(self):
        while self.ongoing:
            print(self.state)
            if self.state == GameStates.INITIALIZE:
                self.initialize()
            if self.state == GameStates.BIDDING:
                self.bidding()
            if self.state == GameStates.CALLING:
                self.calling()
            if self.state == GameStates.PLAYING:
                self.turn()
            self.calculate_total()

if __name__ == '__main__':
    game = Game()
    game.run()
